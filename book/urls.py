from django.urls import path
from . import views

urlpatterns = [
    path('book/', views.hello_clients_text, name='book'),
    path('book_program_lang/', views.book_view, name='book'),
]
