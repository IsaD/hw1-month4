from django.db import models

class BookProgramLang(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='')
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    cost = models.IntegerField()
    color = models.TextField()



def __str__(self):
    return self.title
