from django.http import HttpResponse
from django.shortcuts import render
from . import models

def hello_clients_text(requests):
    return HttpResponse("Здраствуйте,дорогой клиент, вас привествует книжный магазин 'не придумал'")


def book_view(requests):
    book = models.BookProgramLang.objects.all()
    return render(requests, 'book.html', {'book': book})
